﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class guestArrivalList_form : Form
    {
        string check_in_setter;
        string con = @"Data Source=LAPTOP-N5RT3GVU\SQLEXPRESS;Initial Catalog=DBMaxxis;Integrated Security=True";
        public guestArrivalList_form()
        {
            InitializeComponent();
        }

        private void pnl_Data_Info_Paint(object sender, PaintEventArgs e)
        {

        }
        private void guestArrivalList_form_Load(object sender, EventArgs e)
        {
        }
        private void btn_GoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_Informations_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            checkIn();
            WalkinReservation_form frm = new WalkinReservation_form();
            frm.guest_check_in = check_in_setter;
            frm.check_in_ready = 1;
            this.Hide();
            frm.ShowDialog();
            this.Show();
        }
        private void pnl_Data_Info_Enter(object sender, EventArgs e)
        {
            Display();
        }
        //methods
         public void Display()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT id,lastname FROM tbl_guestinfo WHERE NOT checked_in = 1", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_Informations.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_Informations.Rows.Add();
                    dgv_Informations.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_Informations.Rows[n].Cells[1].Value = item[1].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void checkIn()
        {
            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("SELECT * FROM tbl_guestinfo where id = '" + this.dgv_Informations.SelectedRows[0].Cells[0].Value.ToString() + "';", conDatabase);
            conDatabase.Open();
            var reader = cmdDataBase.ExecuteReader();
            try
            {
                reader.Read();
                check_in_setter = reader.GetInt32(0).ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            conDatabase.Close();
        }

        private void guestArrivalList_form_Load_1(object sender, EventArgs e)
        {
            Display();
        }

        private void dgv_Informations_MouseEnter(object sender, EventArgs e)
        {
            Display();
        }
    }
}
