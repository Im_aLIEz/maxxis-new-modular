﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis
{
    public partial class Reservation_Form : Form
    {
        public Reservation_Form()
        {
            InitializeComponent();
        }

        private void Reservation_Form_Load(object sender, EventArgs e)
        {
            webBrowser_Reservation.Navigate("http://maxxhotelmakati.com/booking/reservation.php");
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
