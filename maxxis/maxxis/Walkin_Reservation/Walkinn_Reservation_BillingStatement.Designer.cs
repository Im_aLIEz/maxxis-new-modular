﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkinn_Reservation_BillingStatement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.billingSummary_panel = new System.Windows.Forms.Panel();
            this.lbl_billingSummary = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txt_billingSummary_subTotal = new System.Windows.Forms.TextBox();
            this.lbl_billingSummary_subTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_folioNumber = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.numUD_quantity = new System.Windows.Forms.NumericUpDown();
            this.cbox_add = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgv_billingSummary = new System.Windows.Forms.DataGridView();
            this.col_Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Rate_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_TotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.payments_panel = new System.Windows.Forms.Panel();
            this.txt_payments_paymentTotal = new System.Windows.Forms.TextBox();
            this.lbl_payments_paymentTotal = new System.Windows.Forms.Label();
            this.lbl_payments = new System.Windows.Forms.Label();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.settlementOption_panel = new System.Windows.Forms.Panel();
            this.lbl_settlementOption = new System.Windows.Forms.Label();
            this.lbl_requiredPrePayment = new System.Windows.Forms.Label();
            this.txt_requiredPrePayment = new System.Windows.Forms.TextBox();
            this.lbl_modeOfPayment = new System.Windows.Forms.Label();
            this.rbtn_partialCredit = new System.Windows.Forms.RadioButton();
            this.rbtn_credit = new System.Windows.Forms.RadioButton();
            this.rbtn_cash = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_cashPayment = new System.Windows.Forms.TextBox();
            this.lbl_cashPayment = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.lbl_cardNumber = new System.Windows.Forms.Label();
            this.txt_cardNumber = new System.Windows.Forms.TextBox();
            this.lbl_expire = new System.Windows.Forms.Label();
            this.txt_cvv = new System.Windows.Forms.TextBox();
            this.txt_expire = new System.Windows.Forms.TextBox();
            this.lbl_cvv = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.billingSummary_panel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).BeginInit();
            this.panel5.SuspendLayout();
            this.payments_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            this.panel6.SuspendLayout();
            this.settlementOption_panel.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.billingSummary_panel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 479);
            this.panel1.TabIndex = 0;
            // 
            // billingSummary_panel
            // 
            this.billingSummary_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.billingSummary_panel.Controls.Add(this.lbl_billingSummary);
            this.billingSummary_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.billingSummary_panel.Location = new System.Drawing.Point(0, 0);
            this.billingSummary_panel.Name = "billingSummary_panel";
            this.billingSummary_panel.Size = new System.Drawing.Size(870, 31);
            this.billingSummary_panel.TabIndex = 11;
            // 
            // lbl_billingSummary
            // 
            this.lbl_billingSummary.AutoSize = true;
            this.lbl_billingSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingSummary.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingSummary.Location = new System.Drawing.Point(11, 10);
            this.lbl_billingSummary.Name = "lbl_billingSummary";
            this.lbl_billingSummary.Size = new System.Drawing.Size(126, 14);
            this.lbl_billingSummary.TabIndex = 0;
            this.lbl_billingSummary.Text = "BILLING STATEMENT";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 448);
            this.panel2.TabIndex = 12;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.dgv_billingSummary);
            this.panel3.Controls.Add(this.lbl_folioNumber);
            this.panel3.Controls.Add(this.btn_add);
            this.panel3.Controls.Add(this.numUD_quantity);
            this.panel3.Controls.Add(this.cbox_add);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.txt_cost);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(14, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(831, 296);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel4.Controls.Add(this.txt_billingSummary_subTotal);
            this.panel4.Controls.Add(this.lbl_billingSummary_subTotal);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(829, 31);
            this.panel4.TabIndex = 11;
            // 
            // txt_billingSummary_subTotal
            // 
            this.txt_billingSummary_subTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_billingSummary_subTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_billingSummary_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_billingSummary_subTotal.ForeColor = System.Drawing.Color.White;
            this.txt_billingSummary_subTotal.Location = new System.Drawing.Point(689, 7);
            this.txt_billingSummary_subTotal.Name = "txt_billingSummary_subTotal";
            this.txt_billingSummary_subTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_billingSummary_subTotal.Size = new System.Drawing.Size(137, 17);
            this.txt_billingSummary_subTotal.TabIndex = 74;
            this.txt_billingSummary_subTotal.Text = "Php 0.00";
            // 
            // lbl_billingSummary_subTotal
            // 
            this.lbl_billingSummary_subTotal.AutoSize = true;
            this.lbl_billingSummary_subTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_billingSummary_subTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_billingSummary_subTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_billingSummary_subTotal.Location = new System.Drawing.Point(610, 10);
            this.lbl_billingSummary_subTotal.Name = "lbl_billingSummary_subTotal";
            this.lbl_billingSummary_subTotal.Size = new System.Drawing.Size(75, 14);
            this.lbl_billingSummary_subTotal.TabIndex = 73;
            this.lbl_billingSummary_subTotal.Text = "SUB TOTAL:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label1.Location = new System.Drawing.Point(11, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "BILLING SUMMARY";
            // 
            // lbl_folioNumber
            // 
            this.lbl_folioNumber.AutoSize = true;
            this.lbl_folioNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_folioNumber.Location = new System.Drawing.Point(32, 35);
            this.lbl_folioNumber.Name = "lbl_folioNumber";
            this.lbl_folioNumber.Size = new System.Drawing.Size(33, 17);
            this.lbl_folioNumber.TabIndex = 51;
            this.lbl_folioNumber.Text = "Add";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.White;
            this.btn_add.Location = new System.Drawing.Point(586, 49);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(96, 40);
            this.btn_add.TabIndex = 52;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            // 
            // numUD_quantity
            // 
            this.numUD_quantity.Location = new System.Drawing.Point(370, 61);
            this.numUD_quantity.Name = "numUD_quantity";
            this.numUD_quantity.Size = new System.Drawing.Size(71, 20);
            this.numUD_quantity.TabIndex = 57;
            // 
            // cbox_add
            // 
            this.cbox_add.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_add.FormattingEnabled = true;
            this.cbox_add.Location = new System.Drawing.Point(35, 60);
            this.cbox_add.Name = "cbox_add";
            this.cbox_add.Size = new System.Drawing.Size(299, 21);
            this.cbox_add.TabIndex = 53;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(482, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 56;
            this.label2.Text = "Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cost.Location = new System.Drawing.Point(485, 58);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.ReadOnly = true;
            this.txt_cost.Size = new System.Drawing.Size(71, 23);
            this.txt_cost.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(382, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 17);
            this.label3.TabIndex = 55;
            this.label3.Text = "Qty";
            // 
            // dgv_billingSummary
            // 
            this.dgv_billingSummary.AllowUserToAddRows = false;
            this.dgv_billingSummary.AllowUserToDeleteRows = false;
            this.dgv_billingSummary.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_billingSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_billingSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_billingSummary.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_billingSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_billingSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_billingSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Particulars,
            this.col_Qty,
            this.col_Unit,
            this.col_Rate_Cost,
            this.col_TotalAmount});
            this.dgv_billingSummary.EnableHeadersVisualStyles = false;
            this.dgv_billingSummary.Location = new System.Drawing.Point(14, 96);
            this.dgv_billingSummary.Name = "dgv_billingSummary";
            this.dgv_billingSummary.ReadOnly = true;
            this.dgv_billingSummary.RowHeadersVisible = false;
            this.dgv_billingSummary.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_billingSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_billingSummary.Size = new System.Drawing.Size(803, 180);
            this.dgv_billingSummary.TabIndex = 58;
            // 
            // col_Particulars
            // 
            this.col_Particulars.HeaderText = "Particulars";
            this.col_Particulars.Name = "col_Particulars";
            this.col_Particulars.ReadOnly = true;
            this.col_Particulars.Width = 250;
            // 
            // col_Qty
            // 
            this.col_Qty.HeaderText = "Qty";
            this.col_Qty.Name = "col_Qty";
            this.col_Qty.ReadOnly = true;
            this.col_Qty.Width = 80;
            // 
            // col_Unit
            // 
            this.col_Unit.HeaderText = "Unit";
            this.col_Unit.Name = "col_Unit";
            this.col_Unit.ReadOnly = true;
            // 
            // col_Rate_Cost
            // 
            this.col_Rate_Cost.HeaderText = "Rate/Cost";
            this.col_Rate_Cost.Name = "col_Rate_Cost";
            this.col_Rate_Cost.ReadOnly = true;
            this.col_Rate_Cost.Width = 150;
            // 
            // col_TotalAmount
            // 
            this.col_TotalAmount.HeaderText = "Amount";
            this.col_TotalAmount.Name = "col_TotalAmount";
            this.col_TotalAmount.ReadOnly = true;
            this.col_TotalAmount.Width = 150;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.dgv_payments);
            this.panel5.Controls.Add(this.payments_panel);
            this.panel5.Location = new System.Drawing.Point(14, 365);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(831, 221);
            this.panel5.TabIndex = 1;
            // 
            // payments_panel
            // 
            this.payments_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.payments_panel.Controls.Add(this.txt_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments_paymentTotal);
            this.payments_panel.Controls.Add(this.lbl_payments);
            this.payments_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.payments_panel.Location = new System.Drawing.Point(0, 0);
            this.payments_panel.Name = "payments_panel";
            this.payments_panel.Size = new System.Drawing.Size(829, 31);
            this.payments_panel.TabIndex = 53;
            // 
            // txt_payments_paymentTotal
            // 
            this.txt_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_payments_paymentTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_payments_paymentTotal.ForeColor = System.Drawing.Color.White;
            this.txt_payments_paymentTotal.Location = new System.Drawing.Point(714, 5);
            this.txt_payments_paymentTotal.Name = "txt_payments_paymentTotal";
            this.txt_payments_paymentTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_payments_paymentTotal.Size = new System.Drawing.Size(103, 17);
            this.txt_payments_paymentTotal.TabIndex = 74;
            this.txt_payments_paymentTotal.Text = "0.00";
            // 
            // lbl_payments_paymentTotal
            // 
            this.lbl_payments_paymentTotal.AutoSize = true;
            this.lbl_payments_paymentTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments_paymentTotal.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments_paymentTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments_paymentTotal.Location = new System.Drawing.Point(567, 8);
            this.lbl_payments_paymentTotal.Name = "lbl_payments_paymentTotal";
            this.lbl_payments_paymentTotal.Size = new System.Drawing.Size(141, 14);
            this.lbl_payments_paymentTotal.TabIndex = 73;
            this.lbl_payments_paymentTotal.Text = "PAYMENT TOTAL:    Php";
            // 
            // lbl_payments
            // 
            this.lbl_payments.AutoSize = true;
            this.lbl_payments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_payments.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_payments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_payments.Location = new System.Drawing.Point(11, 8);
            this.lbl_payments.Name = "lbl_payments";
            this.lbl_payments.Size = new System.Drawing.Size(72, 14);
            this.lbl_payments.TabIndex = 0;
            this.lbl_payments.Text = "PAYMENTS";
            // 
            // dgv_payments
            // 
            this.dgv_payments.AllowUserToAddRows = false;
            this.dgv_payments.AllowUserToDeleteRows = false;
            this.dgv_payments.BackgroundColor = System.Drawing.Color.Silver;
            this.dgv_payments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_payments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_payments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_payments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.colAmount,
            this.colDiscount});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_payments.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_payments.EnableHeadersVisualStyles = false;
            this.dgv_payments.Location = new System.Drawing.Point(14, 47);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.ReadOnly = true;
            this.dgv_payments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_payments.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_payments.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_payments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_payments.Size = new System.Drawing.Size(803, 143);
            this.dgv_payments.TabIndex = 54;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // colAmount
            // 
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            this.colAmount.Width = 350;
            // 
            // colDiscount
            // 
            this.colDiscount.HeaderText = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.ReadOnly = true;
            this.colDiscount.Width = 200;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.lbl_modeOfPayment);
            this.panel6.Controls.Add(this.rbtn_partialCredit);
            this.panel6.Controls.Add(this.rbtn_credit);
            this.panel6.Controls.Add(this.rbtn_cash);
            this.panel6.Controls.Add(this.settlementOption_panel);
            this.panel6.Location = new System.Drawing.Point(14, 633);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(830, 379);
            this.panel6.TabIndex = 2;
            // 
            // settlementOption_panel
            // 
            this.settlementOption_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.settlementOption_panel.Controls.Add(this.lbl_settlementOption);
            this.settlementOption_panel.Controls.Add(this.lbl_requiredPrePayment);
            this.settlementOption_panel.Controls.Add(this.txt_requiredPrePayment);
            this.settlementOption_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settlementOption_panel.Location = new System.Drawing.Point(0, 0);
            this.settlementOption_panel.Name = "settlementOption_panel";
            this.settlementOption_panel.Size = new System.Drawing.Size(828, 31);
            this.settlementOption_panel.TabIndex = 11;
            // 
            // lbl_settlementOption
            // 
            this.lbl_settlementOption.AutoSize = true;
            this.lbl_settlementOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_settlementOption.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_settlementOption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_settlementOption.Location = new System.Drawing.Point(11, 8);
            this.lbl_settlementOption.Name = "lbl_settlementOption";
            this.lbl_settlementOption.Size = new System.Drawing.Size(132, 14);
            this.lbl_settlementOption.TabIndex = 0;
            this.lbl_settlementOption.Text = "SETTLEMENT OPTION";
            // 
            // lbl_requiredPrePayment
            // 
            this.lbl_requiredPrePayment.AutoSize = true;
            this.lbl_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_requiredPrePayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_requiredPrePayment.Location = new System.Drawing.Point(551, 4);
            this.lbl_requiredPrePayment.Name = "lbl_requiredPrePayment";
            this.lbl_requiredPrePayment.Size = new System.Drawing.Size(172, 19);
            this.lbl_requiredPrePayment.TabIndex = 42;
            this.lbl_requiredPrePayment.Text = "Required pre-payment";
            // 
            // txt_requiredPrePayment
            // 
            this.txt_requiredPrePayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.txt_requiredPrePayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_requiredPrePayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_requiredPrePayment.ForeColor = System.Drawing.Color.White;
            this.txt_requiredPrePayment.Location = new System.Drawing.Point(728, 6);
            this.txt_requiredPrePayment.Name = "txt_requiredPrePayment";
            this.txt_requiredPrePayment.ReadOnly = true;
            this.txt_requiredPrePayment.Size = new System.Drawing.Size(97, 17);
            this.txt_requiredPrePayment.TabIndex = 43;
            this.txt_requiredPrePayment.Text = "0.00";
            this.txt_requiredPrePayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_modeOfPayment
            // 
            this.lbl_modeOfPayment.AutoSize = true;
            this.lbl_modeOfPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modeOfPayment.Location = new System.Drawing.Point(67, 46);
            this.lbl_modeOfPayment.Name = "lbl_modeOfPayment";
            this.lbl_modeOfPayment.Size = new System.Drawing.Size(144, 19);
            this.lbl_modeOfPayment.TabIndex = 48;
            this.lbl_modeOfPayment.Text = "Mode Of Payment:";
            // 
            // rbtn_partialCredit
            // 
            this.rbtn_partialCredit.AutoSize = true;
            this.rbtn_partialCredit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_partialCredit.Location = new System.Drawing.Point(444, 45);
            this.rbtn_partialCredit.Name = "rbtn_partialCredit";
            this.rbtn_partialCredit.Size = new System.Drawing.Size(111, 21);
            this.rbtn_partialCredit.TabIndex = 47;
            this.rbtn_partialCredit.Text = "Partial Credit";
            this.rbtn_partialCredit.UseVisualStyleBackColor = true;
            // 
            // rbtn_credit
            // 
            this.rbtn_credit.AutoSize = true;
            this.rbtn_credit.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_credit.Location = new System.Drawing.Point(333, 45);
            this.rbtn_credit.Name = "rbtn_credit";
            this.rbtn_credit.Size = new System.Drawing.Size(65, 21);
            this.rbtn_credit.TabIndex = 46;
            this.rbtn_credit.Text = "Credit";
            this.rbtn_credit.UseVisualStyleBackColor = true;
            // 
            // rbtn_cash
            // 
            this.rbtn_cash.AutoSize = true;
            this.rbtn_cash.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_cash.Location = new System.Drawing.Point(231, 45);
            this.rbtn_cash.Name = "rbtn_cash";
            this.rbtn_cash.Size = new System.Drawing.Size(59, 21);
            this.rbtn_cash.TabIndex = 45;
            this.rbtn_cash.Text = "Cash";
            this.rbtn_cash.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.textBox2);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Controls.Add(this.textBox1);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.txt_cashPayment);
            this.panel8.Controls.Add(this.lbl_cashPayment);
            this.panel8.Location = new System.Drawing.Point(34, 79);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(767, 142);
            this.panel8.TabIndex = 49;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(197, 104);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(193, 23);
            this.textBox2.TabIndex = 50;
            this.textBox2.Text = "0.00";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 49;
            this.label4.Text = "Change";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(197, 71);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(193, 23);
            this.textBox1.TabIndex = 48;
            this.textBox1.Text = "0.00";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 19);
            this.label5.TabIndex = 47;
            this.label5.Text = "Cash Handed by Guest";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel9.Controls.Add(this.label6);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(765, 31);
            this.panel9.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label6.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label6.Location = new System.Drawing.Point(11, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "CASH";
            // 
            // txt_cashPayment
            // 
            this.txt_cashPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cashPayment.Location = new System.Drawing.Point(197, 37);
            this.txt_cashPayment.Name = "txt_cashPayment";
            this.txt_cashPayment.ReadOnly = true;
            this.txt_cashPayment.Size = new System.Drawing.Size(193, 23);
            this.txt_cashPayment.TabIndex = 46;
            this.txt_cashPayment.Text = "0.00";
            this.txt_cashPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_cashPayment
            // 
            this.lbl_cashPayment.AutoSize = true;
            this.lbl_cashPayment.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashPayment.Location = new System.Drawing.Point(10, 37);
            this.lbl_cashPayment.Name = "lbl_cashPayment";
            this.lbl_cashPayment.Size = new System.Drawing.Size(115, 19);
            this.lbl_cashPayment.TabIndex = 45;
            this.lbl_cashPayment.Text = "Cash Payment";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.lbl_type);
            this.panel7.Controls.Add(this.cmb_type);
            this.panel7.Controls.Add(this.lbl_cardNumber);
            this.panel7.Controls.Add(this.txt_cardNumber);
            this.panel7.Controls.Add(this.lbl_expire);
            this.panel7.Controls.Add(this.txt_cvv);
            this.panel7.Controls.Add(this.txt_expire);
            this.panel7.Controls.Add(this.lbl_cvv);
            this.panel7.Location = new System.Drawing.Point(34, 239);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(767, 127);
            this.panel7.TabIndex = 50;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel10.Controls.Add(this.label7);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(765, 31);
            this.panel10.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.label7.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.label7.Location = new System.Drawing.Point(11, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "CREDIT";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_type.Location = new System.Drawing.Point(10, 43);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(44, 19);
            this.lbl_type.TabIndex = 28;
            this.lbl_type.Text = "Type";
            // 
            // cmb_type
            // 
            this.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Location = new System.Drawing.Point(106, 43);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(272, 24);
            this.cmb_type.TabIndex = 29;
            // 
            // lbl_cardNumber
            // 
            this.lbl_cardNumber.AutoSize = true;
            this.lbl_cardNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cardNumber.Location = new System.Drawing.Point(10, 87);
            this.lbl_cardNumber.Name = "lbl_cardNumber";
            this.lbl_cardNumber.Size = new System.Drawing.Size(75, 19);
            this.lbl_cardNumber.TabIndex = 30;
            this.lbl_cardNumber.Text = "Card No*";
            // 
            // txt_cardNumber
            // 
            this.txt_cardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cardNumber.Location = new System.Drawing.Point(106, 83);
            this.txt_cardNumber.Name = "txt_cardNumber";
            this.txt_cardNumber.Size = new System.Drawing.Size(272, 23);
            this.txt_cardNumber.TabIndex = 31;
            // 
            // lbl_expire
            // 
            this.lbl_expire.AutoSize = true;
            this.lbl_expire.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_expire.Location = new System.Drawing.Point(397, 84);
            this.lbl_expire.Name = "lbl_expire";
            this.lbl_expire.Size = new System.Drawing.Size(61, 19);
            this.lbl_expire.TabIndex = 32;
            this.lbl_expire.Text = "Expire*";
            // 
            // txt_cvv
            // 
            this.txt_cvv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cvv.Location = new System.Drawing.Point(470, 42);
            this.txt_cvv.Name = "txt_cvv";
            this.txt_cvv.Size = new System.Drawing.Size(106, 23);
            this.txt_cvv.TabIndex = 35;
            // 
            // txt_expire
            // 
            this.txt_expire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_expire.Location = new System.Drawing.Point(470, 83);
            this.txt_expire.Name = "txt_expire";
            this.txt_expire.Size = new System.Drawing.Size(106, 23);
            this.txt_expire.TabIndex = 33;
            // 
            // lbl_cvv
            // 
            this.lbl_cvv.AutoSize = true;
            this.lbl_cvv.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cvv.Location = new System.Drawing.Point(397, 44);
            this.lbl_cvv.Name = "lbl_cvv";
            this.lbl_cvv.Size = new System.Drawing.Size(46, 19);
            this.lbl_cvv.TabIndex = 34;
            this.lbl_cvv.Text = "CVV*";
            // 
            // Walkinn_Reservation_BillingStatement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkinn_Reservation_BillingStatement";
            this.Text = "Walkinn_Reservation_BillingStatement";
            this.panel1.ResumeLayout(false);
            this.billingSummary_panel.ResumeLayout(false);
            this.billingSummary_panel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_billingSummary)).EndInit();
            this.panel5.ResumeLayout(false);
            this.payments_panel.ResumeLayout(false);
            this.payments_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.settlementOption_panel.ResumeLayout(false);
            this.settlementOption_panel.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel billingSummary_panel;
        private System.Windows.Forms.Label lbl_billingSummary;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txt_billingSummary_subTotal;
        private System.Windows.Forms.Label lbl_billingSummary_subTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_folioNumber;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.NumericUpDown numUD_quantity;
        private System.Windows.Forms.ComboBox cbox_add;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_billingSummary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Rate_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_TotalAmount;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel payments_panel;
        private System.Windows.Forms.TextBox txt_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments_paymentTotal;
        private System.Windows.Forms.Label lbl_payments;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDiscount;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel settlementOption_panel;
        private System.Windows.Forms.Label lbl_settlementOption;
        private System.Windows.Forms.Label lbl_requiredPrePayment;
        private System.Windows.Forms.TextBox txt_requiredPrePayment;
        private System.Windows.Forms.Label lbl_modeOfPayment;
        private System.Windows.Forms.RadioButton rbtn_partialCredit;
        private System.Windows.Forms.RadioButton rbtn_credit;
        private System.Windows.Forms.RadioButton rbtn_cash;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_cashPayment;
        private System.Windows.Forms.Label lbl_cashPayment;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.Label lbl_cardNumber;
        private System.Windows.Forms.TextBox txt_cardNumber;
        private System.Windows.Forms.Label lbl_expire;
        private System.Windows.Forms.TextBox txt_cvv;
        private System.Windows.Forms.TextBox txt_expire;
        private System.Windows.Forms.Label lbl_cvv;
    }
}