﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_BusinessSource : Form
    {
        private static string marketplace;
        private static string marketsegment;
        private static string purposeofstay;
        public Walkin_Reservation_BusinessSource()
        {
            InitializeComponent();
        }

        private void Walkin_Reservation_BusinessSource_Load(object sender, EventArgs e)
        {

        }

        private void cbox_marketplace_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormUpdate();
        }

        private void cbox_marketsegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormUpdate();
        }

        private void cbox_purposeofstay_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormUpdate();
        }
        public string MarketPlace
        {
            get { return marketplace; }
            set { marketplace = value; }
        }
        public string MarketSegment
        {
            get { return marketsegment; }
            set { marketsegment = value; }
        }
        public string PurposeOfStay
        {
            get { return purposeofstay; }
            set { purposeofstay = value; }
        }
        public void FormUpdate()
        {
            MarketPlace = cbox_marketplace.Text;
            MarketSegment = cbox_marketplace.Text;
            PurposeOfStay = cbox_purposeofstay.Text;
        }
    }
}
