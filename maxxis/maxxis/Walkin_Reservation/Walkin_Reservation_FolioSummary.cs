﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maxxis.Walkin_Reservation
{
    public partial class Walkin_Reservation_FolioSummary : Form
    {
        Walkin_Reservation_AccomodationDetails WR_accomodationdetails = new Walkin_Reservation_AccomodationDetails();
        Walkin_Reservation_BillingSummary WR_billingsummary = new Walkin_Reservation_BillingSummary();
        Walkin_Reservation_SettlementOption WR_settlement = new Walkin_Reservation_SettlementOption();

        static string overalladult;
        static string overallchildren;
        public string OverallAdult
        {
            get { return overalladult; }
            set { overalladult = value; }
        }
        public string OverallChildren
        {
            get { return overallchildren; }
            set { overallchildren = value; }
        }

        public Walkin_Reservation_FolioSummary()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        internal void Walkin_Reservation_FolioSummary_Load(object sender, EventArgs e)
        {
            if(WR_accomodationdetails.BillChecker==1)
            {
                OverallAdult = (Int32.Parse(WR_accomodationdetails.URadult) + Int32.Parse(WR_accomodationdetails.UCRadult) + Int32.Parse(WR_accomodationdetails.UVRadult)).ToString();
                OverallChildren = (Int32.Parse(WR_accomodationdetails.URchild) + Int32.Parse(WR_accomodationdetails.UCRchild) + Int32.Parse(WR_accomodationdetails.UVRchild)).ToString();
                txt_totalNumberOfAdults.Text = OverallAdult;
                total_numberOfChildren.Text = OverallChildren;

                txt_subTotal.Text = "Php " + WR_billingsummary.SubPayment;
                txt_totalPayment.Text = "Php " + WR_billingsummary.TotalPayment;
                txt_balance.Text = "Php " + (Decimal.Parse(WR_billingsummary.TotalPayment) - Decimal.Parse(WR_settlement.CashPaid));
                txt_folioSummary_balanceDue.Text = "Php " + WR_billingsummary.TotalPayment;
                txt_otherCharges.Text = "Php " + WR_billingsummary.ItemPayment.ToString();
            }
            
        }
    }
}
