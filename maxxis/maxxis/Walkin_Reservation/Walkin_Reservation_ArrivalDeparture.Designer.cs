﻿namespace maxxis.Walkin_Reservation
{
    partial class Walkin_Reservation_ArrivalDeparture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_ArrivalPanel = new System.Windows.Forms.Panel();
            this.pcb_station_arrival = new System.Windows.Forms.PictureBox();
            this.txt_weekdays_arrival = new System.Windows.Forms.TextBox();
            this.dtp_pickupTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_pickupDate = new System.Windows.Forms.DateTimePicker();
            this.pcb_name_arrival = new System.Windows.Forms.PictureBox();
            this.pcb_mode_arrival = new System.Windows.Forms.PictureBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.cmb_station_arrival = new System.Windows.Forms.ComboBox();
            this.cmb_name_arrival = new System.Windows.Forms.ComboBox();
            this.cmb_mode_arrival = new System.Windows.Forms.ComboBox();
            this.txt_number_arrival = new System.Windows.Forms.TextBox();
            this.lbl_weekdays_arrival = new System.Windows.Forms.Label();
            this.lbl_pickupDate = new System.Windows.Forms.Label();
            this.lbl_arrivalDate = new System.Windows.Forms.Label();
            this.lbl_station_arrival = new System.Windows.Forms.Label();
            this.lbl_number_arrival = new System.Windows.Forms.Label();
            this.lbl_name_arrival = new System.Windows.Forms.Label();
            this.lbl_mode_arrival = new System.Windows.Forms.Label();
            this.pnl_ArrInfo = new System.Windows.Forms.Panel();
            this.lbl_ArrivalInformation = new System.Windows.Forms.Label();
            this.pnl_DeparturePanel = new System.Windows.Forms.Panel();
            this.pcb_station_departure = new System.Windows.Forms.PictureBox();
            this.txt_weekdays_departure = new System.Windows.Forms.TextBox();
            this.dtp_dropOffTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_dropOffDate = new System.Windows.Forms.DateTimePicker();
            this.pcb_name_departure = new System.Windows.Forms.PictureBox();
            this.pcb_mode_departure = new System.Windows.Forms.PictureBox();
            this.dtp_departureTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmb_station_departure = new System.Windows.Forms.ComboBox();
            this.cmb_name_departure = new System.Windows.Forms.ComboBox();
            this.cmb_mode_departure = new System.Windows.Forms.ComboBox();
            this.txt_number_departure = new System.Windows.Forms.TextBox();
            this.lbl_weekdays_departure = new System.Windows.Forms.Label();
            this.lbl_dropOffDate = new System.Windows.Forms.Label();
            this.lbl_departureDate = new System.Windows.Forms.Label();
            this.lbl_station_departure = new System.Windows.Forms.Label();
            this.lbl_number_departure = new System.Windows.Forms.Label();
            this.lbl_name_departure = new System.Windows.Forms.Label();
            this.lbl_mode_departure = new System.Windows.Forms.Label();
            this.pnl_Departure = new System.Windows.Forms.Panel();
            this.pnl_DepInfo = new System.Windows.Forms.Label();
            this.pnl_AD_information = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbl_arrivalAndDeparture = new System.Windows.Forms.Label();
            this.pnl_ArrivalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_station_arrival)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_name_arrival)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_mode_arrival)).BeginInit();
            this.pnl_ArrInfo.SuspendLayout();
            this.pnl_DeparturePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_station_departure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_name_departure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_mode_departure)).BeginInit();
            this.pnl_Departure.SuspendLayout();
            this.pnl_AD_information.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_ArrivalPanel
            // 
            this.pnl_ArrivalPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_ArrivalPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_ArrivalPanel.Controls.Add(this.pcb_station_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.txt_weekdays_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.dtp_pickupTime);
            this.pnl_ArrivalPanel.Controls.Add(this.dtp_pickupDate);
            this.pnl_ArrivalPanel.Controls.Add(this.pcb_name_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.pcb_mode_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.dateTimePicker2);
            this.pnl_ArrivalPanel.Controls.Add(this.dateTimePicker3);
            this.pnl_ArrivalPanel.Controls.Add(this.cmb_station_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.cmb_name_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.cmb_mode_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.txt_number_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_weekdays_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_pickupDate);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_arrivalDate);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_station_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_number_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_name_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.lbl_mode_arrival);
            this.pnl_ArrivalPanel.Controls.Add(this.pnl_ArrInfo);
            this.pnl_ArrivalPanel.Location = new System.Drawing.Point(25, 46);
            this.pnl_ArrivalPanel.Name = "pnl_ArrivalPanel";
            this.pnl_ArrivalPanel.Size = new System.Drawing.Size(394, 306);
            this.pnl_ArrivalPanel.TabIndex = 13;
            // 
            // pcb_station_arrival
            // 
            this.pcb_station_arrival.BackColor = System.Drawing.Color.Silver;
            this.pcb_station_arrival.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_station_arrival.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_station_arrival.Location = new System.Drawing.Point(356, 161);
            this.pcb_station_arrival.Name = "pcb_station_arrival";
            this.pcb_station_arrival.Size = new System.Drawing.Size(33, 31);
            this.pcb_station_arrival.TabIndex = 51;
            this.pcb_station_arrival.TabStop = false;
            // 
            // txt_weekdays_arrival
            // 
            this.txt_weekdays_arrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_weekdays_arrival.Location = new System.Drawing.Point(139, 264);
            this.txt_weekdays_arrival.Name = "txt_weekdays_arrival";
            this.txt_weekdays_arrival.Size = new System.Drawing.Size(162, 23);
            this.txt_weekdays_arrival.TabIndex = 48;
            // 
            // dtp_pickupTime
            // 
            this.dtp_pickupTime.CustomFormat = "hh:mm:ss tt";
            this.dtp_pickupTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_pickupTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_pickupTime.Location = new System.Drawing.Point(267, 232);
            this.dtp_pickupTime.Name = "dtp_pickupTime";
            this.dtp_pickupTime.ShowCheckBox = true;
            this.dtp_pickupTime.ShowUpDown = true;
            this.dtp_pickupTime.Size = new System.Drawing.Size(122, 23);
            this.dtp_pickupTime.TabIndex = 47;
            // 
            // dtp_pickupDate
            // 
            this.dtp_pickupDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_pickupDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_pickupDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_pickupDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_pickupDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_pickupDate.Location = new System.Drawing.Point(139, 231);
            this.dtp_pickupDate.Name = "dtp_pickupDate";
            this.dtp_pickupDate.ShowCheckBox = true;
            this.dtp_pickupDate.Size = new System.Drawing.Size(122, 23);
            this.dtp_pickupDate.TabIndex = 46;
            // 
            // pcb_name_arrival
            // 
            this.pcb_name_arrival.BackColor = System.Drawing.Color.Silver;
            this.pcb_name_arrival.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_name_arrival.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_name_arrival.Location = new System.Drawing.Point(356, 82);
            this.pcb_name_arrival.Name = "pcb_name_arrival";
            this.pcb_name_arrival.Size = new System.Drawing.Size(33, 31);
            this.pcb_name_arrival.TabIndex = 50;
            this.pcb_name_arrival.TabStop = false;
            // 
            // pcb_mode_arrival
            // 
            this.pcb_mode_arrival.BackColor = System.Drawing.Color.Silver;
            this.pcb_mode_arrival.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_mode_arrival.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_mode_arrival.Location = new System.Drawing.Point(322, 45);
            this.pcb_mode_arrival.Name = "pcb_mode_arrival";
            this.pcb_mode_arrival.Size = new System.Drawing.Size(33, 31);
            this.pcb_mode_arrival.TabIndex = 49;
            this.pcb_mode_arrival.TabStop = false;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "hh:mm:ss tt";
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(267, 198);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(122, 23);
            this.dateTimePicker2.TabIndex = 45;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePicker3.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dateTimePicker3.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(139, 197);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.ShowCheckBox = true;
            this.dateTimePicker3.Size = new System.Drawing.Size(122, 23);
            this.dateTimePicker3.TabIndex = 44;
            // 
            // cmb_station_arrival
            // 
            this.cmb_station_arrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_station_arrival.FormattingEnabled = true;
            this.cmb_station_arrival.Location = new System.Drawing.Point(139, 164);
            this.cmb_station_arrival.Name = "cmb_station_arrival";
            this.cmb_station_arrival.Size = new System.Drawing.Size(211, 24);
            this.cmb_station_arrival.TabIndex = 21;
            // 
            // cmb_name_arrival
            // 
            this.cmb_name_arrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_name_arrival.FormattingEnabled = true;
            this.cmb_name_arrival.Location = new System.Drawing.Point(139, 85);
            this.cmb_name_arrival.Name = "cmb_name_arrival";
            this.cmb_name_arrival.Size = new System.Drawing.Size(211, 24);
            this.cmb_name_arrival.TabIndex = 20;
            // 
            // cmb_mode_arrival
            // 
            this.cmb_mode_arrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_mode_arrival.FormattingEnabled = true;
            this.cmb_mode_arrival.Location = new System.Drawing.Point(139, 47);
            this.cmb_mode_arrival.Name = "cmb_mode_arrival";
            this.cmb_mode_arrival.Size = new System.Drawing.Size(177, 24);
            this.cmb_mode_arrival.TabIndex = 19;
            // 
            // txt_number_arrival
            // 
            this.txt_number_arrival.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_number_arrival.Location = new System.Drawing.Point(139, 124);
            this.txt_number_arrival.Name = "txt_number_arrival";
            this.txt_number_arrival.Size = new System.Drawing.Size(211, 23);
            this.txt_number_arrival.TabIndex = 17;
            // 
            // lbl_weekdays_arrival
            // 
            this.lbl_weekdays_arrival.AutoSize = true;
            this.lbl_weekdays_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_weekdays_arrival.Location = new System.Drawing.Point(6, 268);
            this.lbl_weekdays_arrival.Name = "lbl_weekdays_arrival";
            this.lbl_weekdays_arrival.Size = new System.Drawing.Size(83, 19);
            this.lbl_weekdays_arrival.TabIndex = 16;
            this.lbl_weekdays_arrival.Text = "Weekdays";
            // 
            // lbl_pickupDate
            // 
            this.lbl_pickupDate.AutoSize = true;
            this.lbl_pickupDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pickupDate.Location = new System.Drawing.Point(6, 236);
            this.lbl_pickupDate.Name = "lbl_pickupDate";
            this.lbl_pickupDate.Size = new System.Drawing.Size(105, 19);
            this.lbl_pickupDate.TabIndex = 15;
            this.lbl_pickupDate.Text = "Pick Up Date";
            // 
            // lbl_arrivalDate
            // 
            this.lbl_arrivalDate.AutoSize = true;
            this.lbl_arrivalDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrivalDate.Location = new System.Drawing.Point(6, 202);
            this.lbl_arrivalDate.Name = "lbl_arrivalDate";
            this.lbl_arrivalDate.Size = new System.Drawing.Size(95, 19);
            this.lbl_arrivalDate.TabIndex = 14;
            this.lbl_arrivalDate.Text = "Arrival Date";
            // 
            // lbl_station_arrival
            // 
            this.lbl_station_arrival.AutoSize = true;
            this.lbl_station_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_station_arrival.Location = new System.Drawing.Point(6, 168);
            this.lbl_station_arrival.Name = "lbl_station_arrival";
            this.lbl_station_arrival.Size = new System.Drawing.Size(61, 19);
            this.lbl_station_arrival.TabIndex = 13;
            this.lbl_station_arrival.Text = "Station";
            // 
            // lbl_number_arrival
            // 
            this.lbl_number_arrival.AutoSize = true;
            this.lbl_number_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_number_arrival.Location = new System.Drawing.Point(6, 128);
            this.lbl_number_arrival.Name = "lbl_number_arrival";
            this.lbl_number_arrival.Size = new System.Drawing.Size(32, 19);
            this.lbl_number_arrival.TabIndex = 12;
            this.lbl_number_arrival.Text = "No.";
            // 
            // lbl_name_arrival
            // 
            this.lbl_name_arrival.AutoSize = true;
            this.lbl_name_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name_arrival.Location = new System.Drawing.Point(4, 89);
            this.lbl_name_arrival.Name = "lbl_name_arrival";
            this.lbl_name_arrival.Size = new System.Drawing.Size(52, 19);
            this.lbl_name_arrival.TabIndex = 11;
            this.lbl_name_arrival.Text = "Name";
            // 
            // lbl_mode_arrival
            // 
            this.lbl_mode_arrival.AutoSize = true;
            this.lbl_mode_arrival.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mode_arrival.Location = new System.Drawing.Point(6, 51);
            this.lbl_mode_arrival.Name = "lbl_mode_arrival";
            this.lbl_mode_arrival.Size = new System.Drawing.Size(49, 19);
            this.lbl_mode_arrival.TabIndex = 10;
            this.lbl_mode_arrival.Text = "Mode";
            // 
            // pnl_ArrInfo
            // 
            this.pnl_ArrInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_ArrInfo.Controls.Add(this.lbl_ArrivalInformation);
            this.pnl_ArrInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_ArrInfo.Location = new System.Drawing.Point(0, 0);
            this.pnl_ArrInfo.Name = "pnl_ArrInfo";
            this.pnl_ArrInfo.Size = new System.Drawing.Size(392, 33);
            this.pnl_ArrInfo.TabIndex = 9;
            // 
            // lbl_ArrivalInformation
            // 
            this.lbl_ArrivalInformation.AutoSize = true;
            this.lbl_ArrivalInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_ArrivalInformation.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ArrivalInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_ArrivalInformation.Location = new System.Drawing.Point(7, 7);
            this.lbl_ArrivalInformation.Name = "lbl_ArrivalInformation";
            this.lbl_ArrivalInformation.Size = new System.Drawing.Size(127, 17);
            this.lbl_ArrivalInformation.TabIndex = 0;
            this.lbl_ArrivalInformation.Text = "Arrival Information";
            // 
            // pnl_DeparturePanel
            // 
            this.pnl_DeparturePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_DeparturePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_DeparturePanel.Controls.Add(this.pcb_station_departure);
            this.pnl_DeparturePanel.Controls.Add(this.txt_weekdays_departure);
            this.pnl_DeparturePanel.Controls.Add(this.dtp_dropOffTime);
            this.pnl_DeparturePanel.Controls.Add(this.dtp_dropOffDate);
            this.pnl_DeparturePanel.Controls.Add(this.pcb_name_departure);
            this.pnl_DeparturePanel.Controls.Add(this.pcb_mode_departure);
            this.pnl_DeparturePanel.Controls.Add(this.dtp_departureTime);
            this.pnl_DeparturePanel.Controls.Add(this.dateTimePicker1);
            this.pnl_DeparturePanel.Controls.Add(this.cmb_station_departure);
            this.pnl_DeparturePanel.Controls.Add(this.cmb_name_departure);
            this.pnl_DeparturePanel.Controls.Add(this.cmb_mode_departure);
            this.pnl_DeparturePanel.Controls.Add(this.txt_number_departure);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_weekdays_departure);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_dropOffDate);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_departureDate);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_station_departure);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_number_departure);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_name_departure);
            this.pnl_DeparturePanel.Controls.Add(this.lbl_mode_departure);
            this.pnl_DeparturePanel.Controls.Add(this.pnl_Departure);
            this.pnl_DeparturePanel.Location = new System.Drawing.Point(441, 47);
            this.pnl_DeparturePanel.Name = "pnl_DeparturePanel";
            this.pnl_DeparturePanel.Size = new System.Drawing.Size(394, 306);
            this.pnl_DeparturePanel.TabIndex = 14;
            // 
            // pcb_station_departure
            // 
            this.pcb_station_departure.BackColor = System.Drawing.Color.Silver;
            this.pcb_station_departure.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_station_departure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_station_departure.Location = new System.Drawing.Point(356, 161);
            this.pcb_station_departure.Name = "pcb_station_departure";
            this.pcb_station_departure.Size = new System.Drawing.Size(33, 31);
            this.pcb_station_departure.TabIndex = 70;
            this.pcb_station_departure.TabStop = false;
            // 
            // txt_weekdays_departure
            // 
            this.txt_weekdays_departure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_weekdays_departure.Location = new System.Drawing.Point(139, 264);
            this.txt_weekdays_departure.Name = "txt_weekdays_departure";
            this.txt_weekdays_departure.Size = new System.Drawing.Size(162, 23);
            this.txt_weekdays_departure.TabIndex = 67;
            // 
            // dtp_dropOffTime
            // 
            this.dtp_dropOffTime.CustomFormat = "hh:mm:ss tt";
            this.dtp_dropOffTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_dropOffTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dropOffTime.Location = new System.Drawing.Point(267, 232);
            this.dtp_dropOffTime.Name = "dtp_dropOffTime";
            this.dtp_dropOffTime.ShowCheckBox = true;
            this.dtp_dropOffTime.ShowUpDown = true;
            this.dtp_dropOffTime.Size = new System.Drawing.Size(122, 23);
            this.dtp_dropOffTime.TabIndex = 66;
            // 
            // dtp_dropOffDate
            // 
            this.dtp_dropOffDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dtp_dropOffDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dtp_dropOffDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_dropOffDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_dropOffDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dropOffDate.Location = new System.Drawing.Point(139, 231);
            this.dtp_dropOffDate.Name = "dtp_dropOffDate";
            this.dtp_dropOffDate.ShowCheckBox = true;
            this.dtp_dropOffDate.Size = new System.Drawing.Size(122, 23);
            this.dtp_dropOffDate.TabIndex = 65;
            // 
            // pcb_name_departure
            // 
            this.pcb_name_departure.BackColor = System.Drawing.Color.Silver;
            this.pcb_name_departure.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_name_departure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_name_departure.Location = new System.Drawing.Point(356, 82);
            this.pcb_name_departure.Name = "pcb_name_departure";
            this.pcb_name_departure.Size = new System.Drawing.Size(33, 31);
            this.pcb_name_departure.TabIndex = 69;
            this.pcb_name_departure.TabStop = false;
            // 
            // pcb_mode_departure
            // 
            this.pcb_mode_departure.BackColor = System.Drawing.Color.Silver;
            this.pcb_mode_departure.BackgroundImage = global::maxxis.Properties.Resources.add;
            this.pcb_mode_departure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_mode_departure.Location = new System.Drawing.Point(322, 45);
            this.pcb_mode_departure.Name = "pcb_mode_departure";
            this.pcb_mode_departure.Size = new System.Drawing.Size(33, 31);
            this.pcb_mode_departure.TabIndex = 68;
            this.pcb_mode_departure.TabStop = false;
            // 
            // dtp_departureTime
            // 
            this.dtp_departureTime.CustomFormat = "hh:mm:ss tt";
            this.dtp_departureTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_departureTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_departureTime.Location = new System.Drawing.Point(267, 198);
            this.dtp_departureTime.Name = "dtp_departureTime";
            this.dtp_departureTime.ShowCheckBox = true;
            this.dtp_departureTime.ShowUpDown = true;
            this.dtp_departureTime.Size = new System.Drawing.Size(122, 23);
            this.dtp_departureTime.TabIndex = 64;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePicker1.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(139, 197);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(122, 23);
            this.dateTimePicker1.TabIndex = 63;
            // 
            // cmb_station_departure
            // 
            this.cmb_station_departure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_station_departure.FormattingEnabled = true;
            this.cmb_station_departure.Location = new System.Drawing.Point(139, 164);
            this.cmb_station_departure.Name = "cmb_station_departure";
            this.cmb_station_departure.Size = new System.Drawing.Size(211, 24);
            this.cmb_station_departure.TabIndex = 62;
            // 
            // cmb_name_departure
            // 
            this.cmb_name_departure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_name_departure.FormattingEnabled = true;
            this.cmb_name_departure.Location = new System.Drawing.Point(139, 85);
            this.cmb_name_departure.Name = "cmb_name_departure";
            this.cmb_name_departure.Size = new System.Drawing.Size(211, 24);
            this.cmb_name_departure.TabIndex = 61;
            // 
            // cmb_mode_departure
            // 
            this.cmb_mode_departure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_mode_departure.FormattingEnabled = true;
            this.cmb_mode_departure.Location = new System.Drawing.Point(139, 47);
            this.cmb_mode_departure.Name = "cmb_mode_departure";
            this.cmb_mode_departure.Size = new System.Drawing.Size(177, 24);
            this.cmb_mode_departure.TabIndex = 60;
            // 
            // txt_number_departure
            // 
            this.txt_number_departure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_number_departure.Location = new System.Drawing.Point(139, 124);
            this.txt_number_departure.Name = "txt_number_departure";
            this.txt_number_departure.Size = new System.Drawing.Size(211, 23);
            this.txt_number_departure.TabIndex = 59;
            // 
            // lbl_weekdays_departure
            // 
            this.lbl_weekdays_departure.AutoSize = true;
            this.lbl_weekdays_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_weekdays_departure.Location = new System.Drawing.Point(6, 268);
            this.lbl_weekdays_departure.Name = "lbl_weekdays_departure";
            this.lbl_weekdays_departure.Size = new System.Drawing.Size(83, 19);
            this.lbl_weekdays_departure.TabIndex = 58;
            this.lbl_weekdays_departure.Text = "Weekdays";
            // 
            // lbl_dropOffDate
            // 
            this.lbl_dropOffDate.AutoSize = true;
            this.lbl_dropOffDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dropOffDate.Location = new System.Drawing.Point(6, 236);
            this.lbl_dropOffDate.Name = "lbl_dropOffDate";
            this.lbl_dropOffDate.Size = new System.Drawing.Size(110, 19);
            this.lbl_dropOffDate.TabIndex = 57;
            this.lbl_dropOffDate.Text = "Drop Off Date";
            // 
            // lbl_departureDate
            // 
            this.lbl_departureDate.AutoSize = true;
            this.lbl_departureDate.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_departureDate.Location = new System.Drawing.Point(6, 202);
            this.lbl_departureDate.Name = "lbl_departureDate";
            this.lbl_departureDate.Size = new System.Drawing.Size(121, 19);
            this.lbl_departureDate.TabIndex = 56;
            this.lbl_departureDate.Text = "Departure Date";
            // 
            // lbl_station_departure
            // 
            this.lbl_station_departure.AutoSize = true;
            this.lbl_station_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_station_departure.Location = new System.Drawing.Point(6, 168);
            this.lbl_station_departure.Name = "lbl_station_departure";
            this.lbl_station_departure.Size = new System.Drawing.Size(61, 19);
            this.lbl_station_departure.TabIndex = 55;
            this.lbl_station_departure.Text = "Station";
            // 
            // lbl_number_departure
            // 
            this.lbl_number_departure.AutoSize = true;
            this.lbl_number_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_number_departure.Location = new System.Drawing.Point(6, 128);
            this.lbl_number_departure.Name = "lbl_number_departure";
            this.lbl_number_departure.Size = new System.Drawing.Size(32, 19);
            this.lbl_number_departure.TabIndex = 54;
            this.lbl_number_departure.Text = "No.";
            // 
            // lbl_name_departure
            // 
            this.lbl_name_departure.AutoSize = true;
            this.lbl_name_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name_departure.Location = new System.Drawing.Point(4, 89);
            this.lbl_name_departure.Name = "lbl_name_departure";
            this.lbl_name_departure.Size = new System.Drawing.Size(52, 19);
            this.lbl_name_departure.TabIndex = 53;
            this.lbl_name_departure.Text = "Name";
            // 
            // lbl_mode_departure
            // 
            this.lbl_mode_departure.AutoSize = true;
            this.lbl_mode_departure.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mode_departure.Location = new System.Drawing.Point(6, 51);
            this.lbl_mode_departure.Name = "lbl_mode_departure";
            this.lbl_mode_departure.Size = new System.Drawing.Size(49, 19);
            this.lbl_mode_departure.TabIndex = 52;
            this.lbl_mode_departure.Text = "Mode";
            // 
            // pnl_Departure
            // 
            this.pnl_Departure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_Departure.Controls.Add(this.pnl_DepInfo);
            this.pnl_Departure.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Departure.Location = new System.Drawing.Point(0, 0);
            this.pnl_Departure.Name = "pnl_Departure";
            this.pnl_Departure.Size = new System.Drawing.Size(392, 33);
            this.pnl_Departure.TabIndex = 9;
            // 
            // pnl_DepInfo
            // 
            this.pnl_DepInfo.AutoSize = true;
            this.pnl_DepInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_DepInfo.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl_DepInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.pnl_DepInfo.Location = new System.Drawing.Point(7, 6);
            this.pnl_DepInfo.Name = "pnl_DepInfo";
            this.pnl_DepInfo.Size = new System.Drawing.Size(149, 17);
            this.pnl_DepInfo.TabIndex = 0;
            this.pnl_DepInfo.Text = "Departure Information";
            // 
            // pnl_AD_information
            // 
            this.pnl_AD_information.AutoScroll = true;
            this.pnl_AD_information.BackColor = System.Drawing.Color.Silver;
            this.pnl_AD_information.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_AD_information.Controls.Add(this.panel5);
            this.pnl_AD_information.Controls.Add(this.pnl_DeparturePanel);
            this.pnl_AD_information.Controls.Add(this.pnl_ArrivalPanel);
            this.pnl_AD_information.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_AD_information.Location = new System.Drawing.Point(0, 0);
            this.pnl_AD_information.Name = "pnl_AD_information";
            this.pnl_AD_information.Size = new System.Drawing.Size(870, 381);
            this.pnl_AD_information.TabIndex = 24;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.panel5.Controls.Add(this.lbl_arrivalAndDeparture);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(868, 31);
            this.panel5.TabIndex = 10;
            // 
            // lbl_arrivalAndDeparture
            // 
            this.lbl_arrivalAndDeparture.AutoSize = true;
            this.lbl_arrivalAndDeparture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_arrivalAndDeparture.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_arrivalAndDeparture.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_arrivalAndDeparture.Location = new System.Drawing.Point(11, 8);
            this.lbl_arrivalAndDeparture.Name = "lbl_arrivalAndDeparture";
            this.lbl_arrivalAndDeparture.Size = new System.Drawing.Size(243, 14);
            this.lbl_arrivalAndDeparture.TabIndex = 0;
            this.lbl_arrivalAndDeparture.Text = "ARRIVAL AND DEPARTURE INFORMATION";
            // 
            // Walkin_Reservation_ArrivalDeparture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(870, 479);
            this.Controls.Add(this.pnl_AD_information);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Walkin_Reservation_ArrivalDeparture";
            this.Text = "Walkin_Reservation_ArrivalDeparture";
            this.pnl_ArrivalPanel.ResumeLayout(false);
            this.pnl_ArrivalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_station_arrival)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_name_arrival)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_mode_arrival)).EndInit();
            this.pnl_ArrInfo.ResumeLayout(false);
            this.pnl_ArrInfo.PerformLayout();
            this.pnl_DeparturePanel.ResumeLayout(false);
            this.pnl_DeparturePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_station_departure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_name_departure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_mode_departure)).EndInit();
            this.pnl_Departure.ResumeLayout(false);
            this.pnl_Departure.PerformLayout();
            this.pnl_AD_information.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_ArrivalPanel;
        private System.Windows.Forms.PictureBox pcb_station_arrival;
        private System.Windows.Forms.TextBox txt_weekdays_arrival;
        private System.Windows.Forms.DateTimePicker dtp_pickupTime;
        private System.Windows.Forms.DateTimePicker dtp_pickupDate;
        private System.Windows.Forms.PictureBox pcb_name_arrival;
        private System.Windows.Forms.PictureBox pcb_mode_arrival;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.ComboBox cmb_station_arrival;
        private System.Windows.Forms.ComboBox cmb_name_arrival;
        private System.Windows.Forms.ComboBox cmb_mode_arrival;
        private System.Windows.Forms.TextBox txt_number_arrival;
        private System.Windows.Forms.Label lbl_weekdays_arrival;
        private System.Windows.Forms.Label lbl_pickupDate;
        private System.Windows.Forms.Label lbl_arrivalDate;
        private System.Windows.Forms.Label lbl_station_arrival;
        private System.Windows.Forms.Label lbl_number_arrival;
        private System.Windows.Forms.Label lbl_name_arrival;
        private System.Windows.Forms.Label lbl_mode_arrival;
        private System.Windows.Forms.Panel pnl_ArrInfo;
        private System.Windows.Forms.Label lbl_ArrivalInformation;
        private System.Windows.Forms.Panel pnl_DeparturePanel;
        private System.Windows.Forms.PictureBox pcb_station_departure;
        private System.Windows.Forms.TextBox txt_weekdays_departure;
        private System.Windows.Forms.DateTimePicker dtp_dropOffTime;
        private System.Windows.Forms.DateTimePicker dtp_dropOffDate;
        private System.Windows.Forms.PictureBox pcb_name_departure;
        private System.Windows.Forms.PictureBox pcb_mode_departure;
        private System.Windows.Forms.DateTimePicker dtp_departureTime;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmb_station_departure;
        private System.Windows.Forms.ComboBox cmb_name_departure;
        private System.Windows.Forms.ComboBox cmb_mode_departure;
        private System.Windows.Forms.TextBox txt_number_departure;
        private System.Windows.Forms.Label lbl_weekdays_departure;
        private System.Windows.Forms.Label lbl_dropOffDate;
        private System.Windows.Forms.Label lbl_departureDate;
        private System.Windows.Forms.Label lbl_station_departure;
        private System.Windows.Forms.Label lbl_number_departure;
        private System.Windows.Forms.Label lbl_name_departure;
        private System.Windows.Forms.Label lbl_mode_departure;
        private System.Windows.Forms.Panel pnl_Departure;
        private System.Windows.Forms.Label pnl_DepInfo;
        private System.Windows.Forms.Panel pnl_AD_information;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_arrivalAndDeparture;
    }
}