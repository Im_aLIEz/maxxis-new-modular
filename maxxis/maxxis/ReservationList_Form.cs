﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maxxis
{
    public partial class ReservationList_Form : Form
    {
        //SQL
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        //variables
        string fName;
        string Lname;

        public ReservationList_Form()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        //methods
        public void Display()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select refno, fname + '" + " "+"' + lname AS GuestName, tnxdate,cin,cout,fname,lname from tbl_booking", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_reservationList.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_reservationList.Rows.Add();
                    dgv_reservationList.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_reservationList.Rows[n].Cells[2].Value = item[1].ToString();
                    dgv_reservationList.Rows[n].Cells[3].Value = item[2].ToString();
                    dgv_reservationList.Rows[n].Cells[4].Value = item[3].ToString();
                    dgv_reservationList.Rows[n].Cells[5].Value = item[4].ToString();
                    dgv_reservationList.Rows[n].Cells[6].Value = item[5].ToString();
                    dgv_reservationList.Rows[n].Cells[7].Value = item[6].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ArrivalDisplay()
        {

            SqlConnection conDatabase = new SqlConnection(con);
            SqlCommand cmdDataBase = new SqlCommand("Select refno, fname + '" + " " + "' + lname AS GuestName, tnxdate,cin,cout,fname,lname from tbl_booking where cin = '"+ dtp_arrivalDate.Text +"'", conDatabase);
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgv_reservationList.Rows.Clear();
                foreach (DataRow item in dt.Rows)
                {
                    int n = dgv_reservationList.Rows.Add();
                    dgv_reservationList.Rows[n].Cells[0].Value = item[0].ToString();
                    dgv_reservationList.Rows[n].Cells[2].Value = item[1].ToString();
                    dgv_reservationList.Rows[n].Cells[3].Value = item[2].ToString();
                    dgv_reservationList.Rows[n].Cells[4].Value = item[3].ToString();
                    dgv_reservationList.Rows[n].Cells[5].Value = item[4].ToString();
                    dgv_reservationList.Rows[n].Cells[6].Value = item[5].ToString();
                    dgv_reservationList.Rows[n].Cells[7].Value = item[6].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ReservationList_Form_Load(object sender, EventArgs e)
        {
            dtp_arrivalDate.Format = DateTimePickerFormat.Custom;
            dtp_arrivalDate.CustomFormat = "yyyy-MM-dd";
            Display();
        }

        private void dgv_reservationList_MouseClick(object sender, MouseEventArgs e)
        {
            txt_reservationNumber.Text = dgv_reservationList.SelectedRows[0].Cells[0].Value.ToString();
            txt_firstName.Text = dgv_reservationList.SelectedRows[0].Cells[6].Value.ToString();
            txt_lastName.Text = dgv_reservationList.SelectedRows[0].Cells[7].Value.ToString();
        }

        private void dtp_arrivalDate_ValueChanged(object sender, EventArgs e)
        {
            ArrivalDisplay();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://maxxhotelmakati.com/booking/reservation.php");
        }

        private void rbt_all_CheckedChanged(object sender, EventArgs e)
        {
            Display();
        }

        private void rbt_active_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btn_checkin_Click(object sender, EventArgs e)
        {

        }
    }
}
