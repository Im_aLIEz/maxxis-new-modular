﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using Microsoft.VisualBasic.FileIO;

namespace maxxis.Front_Desk
{
    public partial class FrontDesk_Form : Form
    {
        string con = System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString;
        string csv_file_path;
        public FrontDesk_Form()
        {
            InitializeComponent();
        }
        
        private void FrontDesk_Form_Load(object sender, EventArgs e)
        {

        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".csv";
            ofd.Filter = "Comma Separated (*.csv)|*.csv";
            ofd.ShowDialog();
            csv_file_path = ofd.FileName;
            DataTable csvData = GetDataTabletFromCSVFile(csv_file_path);
            Console.WriteLine("Rows count:" + csvData.Rows.Count);
            Console.ReadLine();
            txt_path.Text = ofd.FileName;
            Delete();
            InsertDataIntoSQLServerUsingSQLBulkCopy(csvData);
            this.Hide();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void btn_uploadNow_Click(object sender, EventArgs e)
        {
           
        }
        //Methods
        private static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return csvData;
        }
        static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData)
        {
            using (SqlConnection dbConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBMaxxis"].ConnectionString))
            {
                dbConnection.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = "tbl_booking";

                    foreach (var column in csvFileData.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());

                    s.WriteToServer(csvFileData);
                    MessageBox.Show("Data Imported Successfully");
                }
            }
        }
        public void Delete()
        {
            try
            {
                SqlConnection conDatabase = new SqlConnection(con);
                string Query = "delete tbl_booking";
                SqlCommand MyCommand2 = new SqlCommand(Query, conDatabase);
                SqlDataReader MyReader2;
                conDatabase.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                conDatabase.Close();
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
