﻿namespace maxxis.Front_Desk
{
    partial class FrontDesk_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accomo_panel = new System.Windows.Forms.Panel();
            this.lbl_accomodationDetails = new System.Windows.Forms.Label();
            this.pcb_frontDesk = new System.Windows.Forms.PictureBox();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.btn_browse = new System.Windows.Forms.Button();
            this.ofd_openCSV = new System.Windows.Forms.OpenFileDialog();
            this.btn_back = new System.Windows.Forms.Button();
            this.accomo_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_frontDesk)).BeginInit();
            this.SuspendLayout();
            // 
            // accomo_panel
            // 
            this.accomo_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.accomo_panel.Controls.Add(this.lbl_accomodationDetails);
            this.accomo_panel.Controls.Add(this.pcb_frontDesk);
            this.accomo_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.accomo_panel.Location = new System.Drawing.Point(0, 0);
            this.accomo_panel.Name = "accomo_panel";
            this.accomo_panel.Size = new System.Drawing.Size(598, 31);
            this.accomo_panel.TabIndex = 11;
            // 
            // lbl_accomodationDetails
            // 
            this.lbl_accomodationDetails.AutoSize = true;
            this.lbl_accomodationDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_accomodationDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_accomodationDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_accomodationDetails.Location = new System.Drawing.Point(31, 9);
            this.lbl_accomodationDetails.Name = "lbl_accomodationDetails";
            this.lbl_accomodationDetails.Size = new System.Drawing.Size(81, 14);
            this.lbl_accomodationDetails.TabIndex = 0;
            this.lbl_accomodationDetails.Text = "FRONT DESK";
            // 
            // pcb_frontDesk
            // 
            this.pcb_frontDesk.BackgroundImage = global::maxxis.Properties.Resources.Front_Desk;
            this.pcb_frontDesk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcb_frontDesk.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_frontDesk.Location = new System.Drawing.Point(0, 0);
            this.pcb_frontDesk.Name = "pcb_frontDesk";
            this.pcb_frontDesk.Size = new System.Drawing.Size(39, 31);
            this.pcb_frontDesk.TabIndex = 1;
            this.pcb_frontDesk.TabStop = false;
            // 
            // txt_path
            // 
            this.txt_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_path.Location = new System.Drawing.Point(12, 57);
            this.txt_path.Name = "txt_path";
            this.txt_path.Size = new System.Drawing.Size(477, 23);
            this.txt_path.TabIndex = 12;
            // 
            // btn_browse
            // 
            this.btn_browse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_browse.FlatAppearance.BorderSize = 0;
            this.btn_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_browse.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_browse.ForeColor = System.Drawing.Color.White;
            this.btn_browse.Location = new System.Drawing.Point(495, 56);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(91, 23);
            this.btn_browse.TabIndex = 13;
            this.btn_browse.Text = "BROWSE";
            this.btn_browse.UseVisualStyleBackColor = false;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // ofd_openCSV
            // 
            this.ofd_openCSV.FileName = "openFileDialog1";
            // 
            // btn_back
            // 
            this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_back.FlatAppearance.BorderSize = 0;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.ForeColor = System.Drawing.Color.White;
            this.btn_back.Location = new System.Drawing.Point(495, 171);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(91, 23);
            this.btn_back.TabIndex = 15;
            this.btn_back.Text = "BACK";
            this.btn_back.UseVisualStyleBackColor = false;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // FrontDesk_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(598, 206);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.txt_path);
            this.Controls.Add(this.accomo_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrontDesk_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrontDesk_Form";
            this.Load += new System.EventHandler(this.FrontDesk_Form_Load);
            this.accomo_panel.ResumeLayout(false);
            this.accomo_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_frontDesk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel accomo_panel;
        private System.Windows.Forms.Label lbl_accomodationDetails;
        private System.Windows.Forms.PictureBox pcb_frontDesk;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.OpenFileDialog ofd_openCSV;
        private System.Windows.Forms.Button btn_back;
    }
}